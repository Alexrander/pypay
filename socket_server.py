# -*- coding: utf-8 -*-
# 导入socket模块
import socket

# 创建实例
# 默认AF_INET,SOCK_STREAM可以不填写
sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 定义绑定的ip和port
ip_port = ("127.0.0.1", 8000)

# 绑定监听
sk.bind(ip_port)

# 监听
sk.listen(1)

while 1:
    print("等待接受数据..........")
    sock, addr = sk.accept()
    print("已经连接..........")

    while 1:
        data = sock.recv(1024)  # 把接收的数据实例化
        print(data)
