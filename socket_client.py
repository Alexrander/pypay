#!/usr/bin/env python
# -*- coding: utf-8 -*-


import socket

connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip_port = ("127.0.0.1", 12000)
connection.connect(ip_port)

while 1:
    data = connection.recv(1024)
    print(data)
